A  TTT  501     A  GLY  163 protein  3.207
A  TTT  501     A  ASP  164 protein  2.916
A  TTT  501     A  PRO  248 protein  3.359
A  TTT  501     A  TYR  264 protein  3.349
A  TTT  501     A  TYR  268 protein  3.312
A  TTT  501     A  GLN  269 protein  2.828
A  TTT  501     A  TYR  273 protein  3.487
A  TTT  501     A  THR  301 protein  3.405
A  TTT  501     A  HOH  678 unknown  3.469
A  TTT  501     A  HOH  734 unknown  3.257
A   ZN  502     A  CYS  189 protein  3.055
A   ZN  502     A  CYS  192 protein  3.171
A   ZN  502     A  CYS  224 protein  2.560
A   ZN  502     A  CYS  226 protein  2.855
A   ZN  503     A  HIS   17 protein  2.941
A   ZN  503     A  THR   63 protein  3.382
A   ZN  503     A  GLU   67 protein  2.927
A   ZN  504     A  HIS   89 protein  3.167
A   ZN  504     A  ASP  108 protein  2.950
A   ZN  504     A   CL  509 unknown  2.234
A   ZN  505     A  HIS   73 protein  3.254
A   ZN  505     A   CL  507 unknown  2.255
A   ZN  505     A   CL  508 unknown  2.261
A   CL  506     A  THR   10 protein  3.178
A   CL  506     A  VAL   11 protein  2.959
A   CL  506     A  ASP   12 protein  3.464
A   CL  506     A  ASN   15 protein  3.021
A   CL  506     A  GLU   67 protein  2.835
A   CL  507     A   ZN  505 unknown  2.255
A   CL  508     A   ZN  505 unknown  2.261
A   CL  509     A  HIS   89 protein  3.448
A   CL  509     A  ASP  108 protein  3.209
A   CL  509     A   ZN  504 unknown  2.234
A  MES  510     A  ALA   39 protein  3.500
A  MES  510     A  ASN   88 protein  3.244
A  ACT  511     A  TRP  106 protein  3.467
A  ACT  511     A  ASN  109 protein  3.330
A  ACT  511     A  SER  111 protein  3.413
A  ACT  511     A  GLY  271 protein  3.381
A  ACT  511     A  HIS  272 protein  2.681
A  ACT  511     A  HOH  652 unknown  3.371
