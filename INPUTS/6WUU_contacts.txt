C   ZN  501     C  CYS  189 protein  3.113
C   ZN  501     C  CYS  192 protein  3.089
C   ZN  501     C  CYS  224 protein  3.378
C   ZN  501     C  CYS  226 protein  3.303
A   ZN  501     A  CYS  189 protein  3.129
A   ZN  501     A  CYS  192 protein  3.035
A   ZN  501     A  CYS  224 protein  3.341
A   ZN  501     A  CYS  226 protein  3.349
B   ZN  501     B  CYS  189 protein  3.237
B   ZN  501     B  CYS  192 protein  3.053
B   ZN  501     B  CYS  224 protein  3.368
B   ZN  501     B  CYS  226 protein  2.383
B   MG  502     B  GLN  174 protein  3.338
D   ZN  501     D  CYS  189 protein  3.164
D   ZN  501     D  CYS  192 protein  3.231
D   ZN  501     D  CYS  224 protein  3.303
D   ZN  501     D  CYS  226 protein  2.299
D   MG  502     D  ILE  285 protein  3.497
D   MG  502     D  GLY  287 protein  3.070
D   MG  503     D  ASP   61 protein  3.269
