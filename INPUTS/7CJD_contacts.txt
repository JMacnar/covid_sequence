A   ZN  501     A  CYS  189 protein  3.037
A   ZN  501     A  THR  191 protein  3.493
A   ZN  501     A  CYS  192 protein  2.717
A   ZN  501     A  CYS  224 protein  2.646
A   ZN  501     A  CYS  226 protein  2.241
A  EDO  502     A  THR  312 protein  3.380
A  EDO  502     A  HOH  601 unknown  3.259
C   ZN  501     C  LYS  190 protein  3.037
C   ZN  501     C  THR  191 protein  3.300
C   ZN  501     C  CYS  226 protein  3.090
B   ZN  501     B  CYS  189 protein  2.593
B   ZN  501     B  THR  191 protein  2.931
B   ZN  501     B  CYS  192 protein  3.222
B   ZN  501     B  CYS  224 protein  2.809
B   ZN  501     B  CYS  226 protein  2.771
D   ZN  501     D  CYS  189 protein  2.514
D   ZN  501     D  THR  191 protein  3.397
D   ZN  501     D  CYS  192 protein  3.380
D   ZN  501     D  CYS  224 protein  2.667
D   ZN  501     D  CYS  226 protein  2.658
