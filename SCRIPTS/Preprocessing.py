#!/usr/bin/env python
# coding: utf-8
import pandas as pd
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from matplotlib.patches import Patch

# clean the data
sns.set(style="whitegrid")
sns.set_context("paper", font_scale=1.5)

distances_df = pd.DataFrame()

# for index, row in df.iterrows():
# file = "distances.csv"
# molecule_id = row["PDBid"] + " " + row["chain"]
#
# structure_df = pd.read_csv(file, sep=' ', dtype={"Resolution": float, "PQ1(PDB)": float}), parse_dates=["Deposition date"], dayfirst=True, na_values=["NaN"], keep_default_na=False, \
#
# pd.read_fwf(os.path.join("data", file), na_values=["?"], keep_default_na=False,
# widths = [10, 8],
# names=["distance", "CA"])
# structure_df.loc[:, "molecule"] = row["molecule_id"]
# structure_df.loc[:, "chain"] = row["molecule"]
# structure_df.loc[:, "pdbid"] = row["pdbid"]
# structure_df.loc[:, "molecule_id"] = row["no"]
# # structure_df.loc[:, "inhibitor"] = row["Inhibitor"]
# structure_df.loc[:, "group"] = row["Space Group"]
# distances_df = distances_df.append(structure_df)
distances_df = pd.read_csv('distances.csv', sep=',')  # na_values=["NaN"], keep_default_na=False)
distances_df = distances_df.drop_duplicates(subset=["molecule", "CA"], keep="first")
distances_df = distances_df.reset_index(drop=True)

# In[4]:


# clip the diffs at 3A for better visualization
plot_df = distances_df.copy()
plot_df.loc[plot_df.distance > 3, "distance"] = 3
#plot_df.loc[plot_df.distance = "NaN", "distance"] = 7

# In[5]:


# perform pivot to have data in a format suitable for heatmap visualization
heat_data = plot_df.pivot("molecule", "CA", "distance")
heat_data = heat_data.fillna(100)
print(heat_data)
# In[6]:


# rename columns
groups_df = distances_df.loc[:, ["molecule", "chain", "group"]].drop_duplicates()
#groups_df = distances_df.copy(["molecule"])
print(groups_df)


#groups_df = groups_df.rename(columns={"molecule": "chain"})
#groups_df = groups_df.rename(columns={"molecule_id": "molecule"})
groups_df = groups_df.set_index("molecule")
#print(groups_df)

# create palette of colors
space_group_mapping = dict(zip(groups_df["group"].unique(), [
    "#8dd3c7", "#ffffb3", "#bc80bd", "#d96262", "#b3de69", "#bebada",
    "#fdb462", "#fdd4a2", "#ffed6f", "#80bd6f"
]))
space_group_colors = groups_df.group.map(space_group_mapping)

# In[9]:

#ccebc5", "#ffed6f",
   # "#882255"
space_group_mapping_math = {
    f'$C2$': '#8dd3c7', #
    f'$I222$': '#ffffb3', #
    f'$I4_122$': '#bc80bd', #
    f'$P2_1$': '#d96262', #
    f'$P2_12_12$': '#b3de69', #
    f'$P3$': '#bebada', #
    f'$P3_221$': '#fdb462', #
    f'$P4_12_12$': '#fdd4a2',
    f'$P6_222$': '#ffed6f',
    f'$P6_522$': '#80bd6f'
}

# In[ ]:


# inhibitor_mapping = dict(zip(groups_df.Inhibitor.unique(), sns.color_palette("Set2")))
# inhibitor_colors = groups_df.Inhibitor.map(inhibitor_mapping)
chain_mapping = dict(zip(groups_df.chain.unique(), sns.color_palette("Set1")))
chain_colors = groups_df.chain.map(chain_mapping)
# type_mapping = dict(zip(groups_df.type.unique(), ["#BBBBBB", "#009988"]))
# type_colors = groups_df.type.map(type_mapping)
# temp_mapping = dict(zip(groups_df.Temp.unique(), ["#0077BB", "#CC3311"]))
# temp_colors = groups_df.Temp.map(temp_mapping)


# In[10]:
g, ax = plt.subplots()
sns.despine(g, left=True, bottom=True)

# space groups heatmap
for palette in ["flare", "rocket_r", "copper_r", "gist_stern_r", "YlGnBu", "PuBuGn", "cividis_r", "viridis_r", "vlag"]:
    ax = sns.clustermap(heat_data, figsize=(10, 6), col_cluster=False,
                        method="ward", cmap=palette,
                        dendrogram_ratio=(.08, 0),
                        row_colors=space_group_colors,
                        xticklabels=20,
                        yticklabels=1,
                        mask=(heat_data==100),
                        cbar_pos=(-.10, 0.5, 0.03, .15))
    #handles = [Patch(facecolor=space_group_mapping_math[name]) for name in space_group_mapping_math]

    for label in space_group_mapping:
        ax.ax_col_dendrogram.bar(0, 0, color=space_group_mapping_math[label],
                                label=label, linewidth=0)
    ax.ax_col_dendrogram.legend(loc="lower left", ncol=5, title="Space group")
    #plt.legend(handles, space_group_mapping_math, title='Space group',
    #           bbox_to_anchor=(1, 1), bbox_transform=plt.gcf().transFigure, loc='upper right')
    #plt.xlabel(r'C-alpha')
    plt.ylabel("distance [A]")
    ax.ax_heatmap.set_xlabel(r'Residue number')
    ax.ax_heatmap.set_ylabel("Molecule")
    ax.ax_heatmap.set_yticklabels(ax.ax_heatmap.get_ymajorticklabels(), fontsize=10)
    #plt.setp(ax.ax_heax.ax_heatmap.set_ylabelatmap.xaxis.set_xtickslabels(x_labels), rotation=0)
    #ax.set_xticklabels(x_labels)
    #plt.xticks(np.arange(0, 321, step=20), x_labels)
    ax.savefig("space_group_" + palette + ".png", dpi=600)
    ax.savefig("space_group_" + palette + ".pdf", dpi=600)

# In[ ]:


# temp heatmap
# ax = sns.clustermap(heat_data, figsize=(10, 10), col_cluster=False,
#                    method = "ward", cmap="flare",
#                    dendrogram_ratio=(.15, 0),
#                    row_colors=temp_colors,
#                    cbar_pos=(0.02, .08, .03, .4))
# ax.savefig("temperature.png")


# In[12]:


# apo-holo heatmap
# ax = sns.clustermap(heat_data, figsize=(10, 10), col_cluster=False,
#                    method = "ward", cmap="flare",
#                    dendrogram_ratio=(.15, 0),
#                    row_colors=type_colors,
#                    cbar_pos=(0.02, .08, .03, .4))
# ax.savefig("apo_holo.png")
