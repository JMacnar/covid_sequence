import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv


def numpy_fillna(data):
    # Get lengths of each row of data
    lens = np.array([len(i) for i in data])

    # Mask of valid places in each row
    mask = np.arange(lens.max()) < lens[:, None]

    # Setup output array and put elements from data into masked positions
    out = np.zeros(mask.shape, dtype=data.dtype)
    out[mask] = np.concatenate(data)
    return out


data_file = '../OUTPUTS/Resolution_PQ1_date.csv'
data = pd.read_csv(data_file, sep=',', dtype={"Resolution": float, "PQ1": float})
data.head(n=3)
sns.set(style="whitegrid")
sns.set_context("paper", font_scale=1.5)

# for regplot
#f, ax = plt.subplots(figsize=(7.5, 7.5))

#for with different point styles 
g, ax = plt.subplots(figsize=(10.0, 7.5))
sns.despine(g, left=True, bottom=True)
# for plot with regresion line
#sns.regplot(data=data, x="Resolution", y="PQ1", logx=True, ci=None, scatter_kws={'s':150}, color='xkcd:orange')


#for plot with different point styles
# g = (g.set_axis_labels("bla", "Bla").set(xlim=(1, 3.25), ylim=(0, 100)))
#g.set(xlabel="Resolution [A]", ylabel="P(Q1)", ylim=(0, 100), xlim=(1.0, 3.25))
#plt.xlabel("Resolution [A]")
#plt.ylabel("P(Q1)")
#plt.ylim(0, 100)
#plt.xlim(1.0, 3.25)

#markers = {"none":"o", "minimal":"s", "moderate":"P", "significant":"X"}
markers = {"Main Protease":"o", "PLpro":"s"}
sns.scatterplot(data=data, x="Resolution", y="PQ1", hue="Issues", style="Protein", hue_order=["none", "minimal", "moderate", "significant"], markers=markers, s=150, alpha=0.80, palette=["gray", "green", "orange", "red"])

# for plot with regresion line
ax.set_xlabel("Resolution [A]")
ax.set_ylabel("P(Q1)")
ax.set_xlim(xmin=1.0, xmax=3.25)
ax.set_ylim(ymin=0, ymax=100)

#plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0.)


# fig = plt.figure(num=None, figsize=(10, 10), dpi=80, facecolor='w', edgecolor='k')
# cmap = sns.cubehelix_palette(light=1, as_cmap=True)
# labels = pd.read_csv('../OUTPUTS/X-ray_sequences.txt', delimiter=',')
# res = sns.heatmap(df, annot=labels, vmin=0.0, vmax=100.0,
#                  fmt='', cmap=cmap, cbar_kws={"shrink": .82},
#                  linewidths=0.1, linecolor='gray')
# res.invert_yaxis()
lgnd = plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', scatterpoints=1, markerscale=2.5, borderaxespad=0.)
g.subplots_adjust(right=0.70)
plt.savefig('PQ1_resolution_protein_.svg', dpi=600)
plt.show()

print("done")
