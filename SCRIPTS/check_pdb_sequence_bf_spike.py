import os
import sys
from functools import reduce
import numpy as np
import requests
import pandas as pd

sys.path.append("/home/asia/DGront/bioshell/bin")
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/DGront/bioshell/data'

from pybioshell.core.data.io import Pdb
from pybioshell.core.chemical import monomer_type_name


def get_fasta_and_b_factors(pdb_fname, chain_id):
    structure = load_structure(pdb_fname)
    pdbid = pdb_fname.rsplit(".", 1)[0]
    # print(structure.is_xray(), structure.is_em(), structure.is_nmr())
    if structure.is_xray():
        get_sequence_and_b_factors(chain_id, pdbid, structure, 'X-ray')
    if structure.is_em():
        get_sequence_and_b_factors(chain_id, pdbid, structure, 'EM')
    ligand_list = list_ligands(chain_id, structure)
    for l in ligand_list:
        list_contacts(l, chain_id, structure)


def get_sequence_and_b_factors(chain_id, pdbid, structure, method):
    for code in structure.chain_codes():
        if code != chain_id:
            continue
        else:
            chain = iter_chains(structure, chain_id, method)
            bf_mins = []
            bf_maxs = []
            bf_avgs = []
            prev_residue = chain[0]
            if prev_residue.id() != -5:
                with open(str(method + '_sequences.txt'), 'a') as f:
                    for j in range(prev_residue.id()):
                        print('-', end=',', file=f)
                with open(str(method + '_sequences.fasta'), 'a') as w:
                    print('-' * (prev_residue.id()), sep=',', end='', file=w)
            for i in range(prev_residue.id()):
                bf_mins.append('NaN')
                bf_maxs.append('NaN')
                bf_avgs.append('NaN')
        for ires in range(int(chain.terminal_residue_index())+1):
            residue = chain[ires]
            difference = residue.id() - prev_residue.id()
            if difference > 1:
                with open(str(method + '_sequences.txt'), 'a') as f:
                    for i in range(difference - 1):
                        print('-', end=',', file=f)
                with open(str(method + '_sequences.fasta'), 'a') as w:
                    print('-' * (difference - 1), end='', file=w)
                for i in range(difference - 1):
                    bf_mins.append('NaN')
                    bf_maxs.append('NaN')
                    bf_avgs.append('NaN')
            letter_code = residue.residue_type().code1
            with open(str(method + '_sequences.txt'), 'a') as f:
                print(letter_code, end=',', file=f)
            with open(str(method + '_sequences.fasta'), 'a') as w:
                print(letter_code, end='', file=w)
            bf_min, bf_max, bf_avg = iter_atoms(residue)
            bf_mins.append(bf_min)
            bf_maxs.append(bf_max)
            bf_avgs.append(bf_avg)
            # print(prev_residue.id(), residue.id(), difference)
            prev_residue = residue
        with open(str(method + '_b_mins.txt'), 'a') as b_min:
            print(pdbid, chain_id, structure.deposition_date(), *bf_mins, sep=',', file=b_min)
        with open(str(method + '_b_maxs.txt'), 'a') as b_max:
            print(pdbid, chain_id, structure.deposition_date(), *bf_mins, sep=',', file=b_max)
        with open(str(method + '_b_avgs.txt'), 'a') as b_avg:
            print(pdbid, chain_id, structure.deposition_date(), *bf_mins, sep=',', file=b_avg)
        with open(str(method + '_sequences.txt'), 'a') as f:
            print('', file=f)
        with open(str(method + '_sequences.fasta'), 'a') as w:
            print('', file=w)


#        print(';', *bf_mins, ';', *bf_maxs, ';', *bf_avgs)


def load_structure(pdb_fname):
    structure = Pdb(pdb_fname, "", True).create_structure(0)
    return structure


def iter_chains(bioshell_structure, code, method):
    chain = bioshell_structure.get_chain(code)
    with open(str(method + "_sequences.txt"), 'a') as f:
        print(bioshell_structure.code(), chain.id(), bioshell_structure.deposition_date(), sep=',', end=',', file=f)
    with open(str(method + "_sequences.fasta"), 'a') as w:
        print('>' + bioshell_structure.code(), chain.id(), sep='_', file=w)
    # print(chain.create_sequence().sequence)
    return chain


def iter_atoms(bioshell_residue):
    atoms_bf = []
    for atom in range(bioshell_residue.count_atoms()):
        atom_bf = bioshell_residue[atom].b_factor()  # residue_type().n_atoms):
        atoms_bf.append("{:3.0f}".format(float(atom_bf)))
    return min(atoms_bf), max(atoms_bf), round(avg(atoms_bf), 2)


def avg(lst):
    return reduce(lambda a, b: float(a) + float(b), lst) / len(lst)


def download_pdb(pdb_code):
    try:
        r = requests.get("https://files.rcsb.org/view/%s.pdb" % pdb_code, allow_redirects=True)
        open(pdb_code + ".pdb", "wb").write(r.content)
    except Exception as error:
        print("An issue occurred during", pdb_code, "downloading", error)


def remove_chars(inpt, which_ones=',()"'):
    for c in list(which_ones):
        inpt = inpt.replace(c, "")
    return inpt


def process_list_file(fname):
    # data = np.loadtxt(fname, dtype=str, delimiter=',', skiprows=1, usecols=(0,))
    # data = pd.read_csv(fname, delimiter=',',
    #                   usecols=['PDB', 'Resol.', 'Released', 'Method', 'Ligand IDs', 'Deposition Date'])
    data = pd.read_csv(fname, delimiter=',', usecols=['PDB', 'chain'])
    print(data)
    for pdb_fname, chain in data.itertuples(index=False):
        token = remove_chars(pdb_fname)
        download_pdb(token)
        pdb_file = token + ".pdb"
        get_fasta_and_b_factors(pdb_file, chain)
        os.remove(pdb_file)


def remove_old_outputs():
    for output_file in {'EM_sequences.fasta', 'EM_sequences.txt', 'EM_b_mins.txt', 'EM_b_maxs.txt', 'EM_b_avgs.txt',
                        'X-ray_sequences.fasta', 'X-ray_sequences.txt', 'X-ray_b_mins.txt', 'X-ray_b_maxs.txt',
                        'X-ray_b_avgs.txt', '*_contacts.txt'}:
        try:
            os.remove(output_file)
        except OSError:
            pass


def list_ligands(chain_id, structure):
    ligands = []
    for code in structure.chain_codes():
        if code != chain_id:
            continue
        else:
            chain = structure.get_chain(code)
            for ir in range(chain.terminal_residue_index() + 1, chain.size()):
                resid = chain[ir]
                code3 = resid.residue_type().code3
                if resid.residue_type().code3 == "HOH": continue  # Skip water molecules, they are so obvious and abundant
                formula = structure.formula(code3)
                hetname = structure.hetname(code3)
                ligands.append(resid)
                # print("%3s %c %4d %s %s" % (code3, chain.id(), resid.id(), formula.strip(), hetname.strip()))
    return ligands


def list_contacts(ligand, chain_id, structure):
    cutoff = 3.5
    for code in structure.chain_codes():
        if code != chain_id:
            continue
        else:
            chain = structure.get_chain(code)
            for ir in range(chain.count_residues()):
                res = chain[ir]
                if res == ligand:
                    continue
                d = res.min_distance(ligand)  # ---- If residue is close enough to ligand
                if d < cutoff:
                    flaga = False
                    for ilig in range(ligand.count_atoms()):
                        for ioth in range(res.count_atoms()):
                            ligand_atom = ligand[ilig]
                            other_atom = res[ioth]
                            if ligand_atom.distance_to(other_atom) <= cutoff and flaga == False:
                                with open(structure.code() + '_contacts.txt', 'a') as c:
                                    print('{}  {:>3} {:4d}     {}  {:>3} {:4d} {} {:6.3f}'.format(
                                        ligand.owner().id(),
                                        ligand.residue_type().code3,
                                        ligand.id(),
                                        res.owner().id(),
                                        res.residue_type().code3,
                                        res.id(),
                                        monomer_type_name(res.residue_type()),
                                        ligand_atom.distance_to(other_atom)), file=c)
                                flaga = True


if __name__ == '__main__':
    # get_fasta_and_b_factors('4mcb.pdb')
    if len(sys.argv) == 1:
        remove_old_outputs()
        process_list_file('../INPUTS/ppl-matches-in-PDB-2020-11-19_95.csv')
        # 'Homologs-to-spike-protein-in-PDB-2020-11-17_mwe.csv')
    else:
        remove_old_outputs()
        process_list_file(sys.argv[1])
