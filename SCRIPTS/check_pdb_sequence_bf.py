import os
import sys
from functools import reduce
import numpy as np
import requests

sys.path.append("/home/asia/DGront/bioshell/bin")
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/DGront/bioshell/data'

from pybioshell.core.data.io import Pdb


def get_fasta_and_b_factors(pdb_fname):
    structure = load_structure(pdb_fname)
    for code in structure.chain_codes():
        chain = iter_chains(structure, code)
        bf_mins = []
        bf_maxs = []
        bf_avgs = []
        prev_residue = chain[0]
        if prev_residue.id() != -5:
            print('-' * (prev_residue.id()+5), end='')
            for i in range(prev_residue.id()+6):
                bf_mins.append('--.--')
                bf_maxs.append('--.--')
                bf_avgs.append('--.--')
        for ires in range(int(chain.terminal_residue_index())):
            residue = chain[ires]
            difference = residue.id() - prev_residue.id()
            if difference > 1:
                print('-' * (difference - 1), end='')
                for i in range(difference):
                    bf_mins.append('--.--')
                    bf_maxs.append('--.--')
                    bf_avgs.append('--.--')

            letter_code = residue.residue_type().code1
            print(letter_code, end='')
            bf_min, bf_max, bf_avg = iter_atoms(residue)
            bf_mins.append(bf_min)
            bf_maxs.append(bf_max)
            bf_avgs.append(bf_avg)
            # print(prev_residue.id(), residue.id(), difference)
            prev_residue = residue
        print(';', *bf_mins, ';', *bf_maxs, ';', *bf_avgs)


def load_structure(pdb_fname):
    structure = Pdb(pdb_fname, "", False).create_structure(0)
    return structure


def iter_chains(bioshell_structure, code):
    chain = bioshell_structure.get_chain(code)
    print(bioshell_structure.code(), chain.id(), end=' ')
    # print(chain.create_sequence().sequence)
    return chain


def iter_atoms(bioshell_residue):
    atoms_bf = []
    for atom in range(bioshell_residue.count_atoms()):
        atom_bf = bioshell_residue[atom].b_factor()  # residue_type().n_atoms):
        atoms_bf.append("{:3.2f}".format(float(atom_bf)))
    return min(atoms_bf), max(atoms_bf), round(avg(atoms_bf), 2)


def avg(lst):
    return reduce(lambda a, b: float(a) + float(b), lst) / len(lst)


def download_pdb(pdb_code):
    try:
        r = requests.get("https://files.rcsb.org/view/%s.pdb" % pdb_code, allow_redirects=True)
        open(pdb_code + ".pdb", "wb").write(r.content)
    except Exception as error:
        print("An issue occure during", pdb_code, "downloading", error)


def remove_chars(inpt, which_ones=',()"'):
    for c in list(which_ones):
        inpt = inpt.replace(c, "")
    return inpt


def process_list_file(fname):
    files = np.loadtxt(fname, dtype=str, delimiter=',', skiprows=1, usecols=(1,))
    for pdb_fname in files:
        token = remove_chars(pdb_fname)
        download_pdb(token)
        pdb_file = token + ".pdb"
        get_fasta_and_b_factors(pdb_file)
        os.remove(pdb_file)


if __name__ == '__main__':
    # get_fasta_and_b_factors('4mcb.pdb')
    if len(sys.argv) == 1:
        process_list_file('../INPUTS/SARS-CoV-2_related_structures_mwe.csv')
    else:
        process_list_file(sys.argv[1])
