import sys, math, os
sys.path.append("/home/asia/DGront/bioshell/bin")
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/DGront/bioshell/data'

from pybioshell.core.data.io import Pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()

"""Script based on original PyBioShell script (ref to readthedocs)"""

rms = CrmsdOnVec3()
list_of_pdbs = ['../INPUTS/6W9C_A.pdb',  '../INPUTS/6WUU_A.pdb',  '../INPUTS/6WZU_A.pdb',  '../INPUTS/6XAA_A.pdb',  '../INPUTS/6YVA_A.pdb',  '../INPUTS/7CJM_B.pdb',  '../INPUTS/7D47_A.pdb',  '../INPUTS/7D7T_A.pdb',  '../INPUTS/7JIT_A.pdb',  '../INPUTS/7JIW_A.pdb',  '../INPUTS/7JRN_A.pdb',  '../INPUTS/7KOK_A.pdb', '../INPUTS/6WRH_A.pdb',  '../INPUTS/6WX4_D.pdb',  '../INPUTS/6XA9_A.pdb',  '../INPUTS/6XG3_A.pdb',  '../INPUTS/7CJD_A.pdb',  '../INPUTS/7CMD_A.pdb',  '../INPUTS/7D6H_A.pdb',  '../INPUTS/7JIR_A.pdb',  '../INPUTS/7JIV_A.pdb',  '../INPUTS/7JN2_A.pdb',  '../INPUTS/7KOJ_A.pdb',  '../INPUTS/7KOL_A.pdb']
if len(list_of_pdbs) == 2:  # --- The case of each-vs-each calculations between models of a single PDB file

    pdb = Pdb(sys.argv[1], "is_ca", False)
    n_atoms = pdb.count_atoms(0)

    structure = pdb.create_structure(0)
    models = []

    for i_model in range(0, pdb.count_models()):
        xyz = vector_core_data_basic_Vec3()
        for i in range(n_atoms): xyz.append(Vec3())
        models.append(xyz)

    for i_model in range(0, pdb.count_models()):
        pdb.fill_structure(i_model, models[i_model])
        for j in range(i_model):
            try:
                print("%2d %2d %6.3f" % (i_model, j, rms.crmsd(models[i_model], models[j], len(models[j]))))
            except:
                sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))

else:  # --- The case when two or more PDB files are given, calculates first vs. the rest

    pdb = Pdb(list_of_pdbs[0], "is_ca", False)
    n_atoms = pdb.count_atoms(0)
    structure = pdb.create_structure(0)

    xyz = vector_core_data_basic_Vec3()
    for i in range(n_atoms): xyz.append(Vec3())
    pdb.fill_structure(0, xyz)

    for pdb_fname in list_of_pdbs[1:]:
        other_pdb = Pdb(pdb_fname, "is_ca", False)
        other_structure = other_pdb.create_structure(0)
        if n_atoms != other_pdb.count_atoms(0):
            print("The two structures have different number of CA atoms!\n")
        other_xyz = vector_core_data_basic_Vec3()
        for i in range(n_atoms): other_xyz.append(Vec3())
        other_pdb.fill_structure(0, other_xyz)
        try:
            print("%s: %6.3f" % (pdb_fname.split("/")[-1].split(".")[0], rms.crmsd(xyz, other_xyz, n_atoms)))
        except:
            sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))

