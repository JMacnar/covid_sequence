import sys
import os
import numpy as np

sys.path.append("/home/asia/DGront/bioshell/bin")
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/DGront/bioshell/data'
from pybioshell.core.data.io import Pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
# from pybioshell.core.alignment import SequenceNWAligner, SequenceSWAligner
# from pybioshell.core.data.io import read_fasta_file
from pybioshell.utils import LogManager

LogManager.INFO()

"""
Script based on original PyBioShell script for crmsd calculation (ref to readthedocs)

ATTENTION! Scripts assumes that proteins are identical and have the same numerations of amino acids in pdb files. 
First file from the list is a reference (medoid) from which only first chain is taken. For the rest of the files all 
chains are taken into the account. 

"""

rms = CrmsdOnVec3()
#list_of_pdbs = ['../INPUTS/7jiv.cif.pdb.pdb', '../INPUTS/6w9c.cif.pdb.pdb']
list_of_pdbs = ["../INPUTS/7jiv.cif.pdb.pdb", "../INPUTS/6w9c.cif.pdb.pdb", "../INPUTS/6wrh.cif.pdb.pdb", "../INPUTS/6yva.cif.pdb.pdb", "../INPUTS/6wuu.cif.pdb.pdb", "../INPUTS/6wx4.cif.pdb.pdb", "../INPUTS/6wzu.cif.pdb.pdb", "../INPUTS/6xa9.cif.pdb.pdb", "../INPUTS/6xaa.cif.pdb.pdb", "../INPUTS/6xg3.cif.pdb.pdb", "../INPUTS/7jir.cif.pdb.pdb", "../INPUTS/7jit.cif.pdb.pdb", "../INPUTS/7jiw.cif.pdb.pdb", "../INPUTS/7jn2.cif.pdb.pdb", "../INPUTS/7jrn.cif.pdb.pdb", "../INPUTS/7cjd.cif.pdb.pdb", "../INPUTS/7cjm.cif.pdb.pdb", "../INPUTS/7cmd.cif.pdb.pdb", "../INPUTS/7d47.cif.pdb.pdb", "../INPUTS/7d6h.cif.pdb.pdb", "../INPUTS/7d7t.cif.pdb.pdb", "../INPUTS/7koj.cif.pdb.pdb", "../INPUTS/7kok.cif.pdb.pdb", "../INPUTS/7kol.cif.pdb.pdb", "../INPUTS/7krx.cif.pdb.pdb"]
#list_of_pdbs = ["../INPUTS/7jiv.cif.pdb_A.pdb", "../INPUTS/6w9c.cif.pdb_A.pdb", "../INPUTS/6w9c.cif.pdb_B.pdb", "../INPUTS/6w9c.cif.pdb_C.pdb", "../INPUTS/6wrh.cif.pdb_A.pdb", "../INPUTS/6wuu.cif.pdb_A.pdb", "../INPUTS/6wuu.cif.pdb_B.pdb", "../INPUTS/6wuu.cif.pdb_C.pdb", "../INPUTS/6wuu.cif.pdb_D.pdb", "../INPUTS/6wx4.cif.pdb_D.pdb", "../INPUTS/6wzu.cif.pdb_A.pdb", "../INPUTS/6xa9.cif.pdb_A.pdb", "../INPUTS/6xa9.cif.pdb_C.pdb", "../INPUTS/6xa9.cif.pdb_E.pdb", "../INPUTS/6xaa.cif.pdb_A.pdb", "../INPUTS/6xg3.cif.pdb_A.pdb", "../INPUTS/6yva.cif.pdb_A.pdb", "../INPUTS/7cjd.cif.pdb_A.pdb", "../INPUTS/7cjd.cif.pdb_B.pdb", "../INPUTS/7cjd.cif.pdb_C.pdb", "../INPUTS/7cjd.cif.pdb_D.pdb", "../INPUTS/7cjm.cif.pdb_B.pdb", "../INPUTS/7cmd.cif.pdb_A.pdb", "../INPUTS/7cmd.cif.pdb_B.pdb", "../INPUTS/7cmd.cif.pdb_C.pdb", "../INPUTS/7cmd.cif.pdb_D.pdb", "../INPUTS/7d47.cif.pdb_A.pdb", "../INPUTS/7d47.cif.pdb_B.pdb", "../INPUTS/7d6h.cif.pdb_A.pdb", "../INPUTS/7d7t.cif.pdb_A.pdb", "../INPUTS/7d7t.cif.pdb_B.pdb", "../INPUTS/7jir.cif.pdb_A.pdb", "../INPUTS/7jit.cif.pdb_A.pdb", "../INPUTS/7jiw.cif.pdb_A.pdb", "../INPUTS/7jn2.cif.pdb_A.pdb", "../INPUTS/7jrn.cif.pdb_A.pdb", "../INPUTS/7jrn.cif.pdb_J.pdb", "../INPUTS/7koj.cif.pdb_A.pdb", "../INPUTS/7kok.cif.pdb_A.pdb", "../INPUTS/7kol.cif.pdb_A.pdb", "../INPUTS/7krx.cif.pdb_A.pdb"]

# def calculate_rmsd(residue_atom, other_residue_atom):
# x = residue_atom.x - other_residue_atom.x
# y = residue_atom.y - other_residue_atom.y
# z = residue_atom.z - other_residue_atom.z
# rmsd = np.sqrt(x * x + y * y + z * z)
# print("RMSD", rmsd)
# return rmsd


def get_sequence(bioshell_structure, chain_id):
    bioshell_chain = bioshell_structure.get_chain(chain_id)
    prev_residue = bioshell_chain[0]
    fasta_list = []
    coordinates_list = []
    residue_list = []
    if prev_residue.id() != -5:
        for i in range(prev_residue.id() + 5):
            fasta_list.append('-')
            coordinates_list.append('-')
            residue_list.append('-')
    for ires in range(int(bioshell_chain.terminal_residue_index()) + 1):
        residue = bioshell_chain[ires]
        difference = residue.id() - prev_residue.id()
        if difference > 1:
            # print("difference", difference)
            # with open(str(method + '_sequences.fasta'), 'a') as w:
            #    print('-' * (difference - 1), end='', file=w)
            for i in range(difference - 1):
                fasta_list.append('-')
                coordinates_list.append('-')
                residue_list.append('-')
        letter_code = residue.residue_type().code1
        fasta_list.append(letter_code)
        coordinates_list.append(residue)
        residue_list.append(residue.id())
        # print(prev_residue.id(), residue.id(), difference)
        prev_residue = residue
    fasta_string = ''.join(fasta_list)
    w = str("> " + bioshell_structure.code() + "_" + bioshell_chain.id() + "\n" + fasta_string)
    print(w)
    # print(residue_list)
    return fasta_list, coordinates_list, residue_list


def remove_old_outputs(filename):
    try:
        os.remove(filename)
    except OSError:
        pass


if __name__ == '__main__':
    output_crmsd_file = 'distances.csv'
    remove_old_outputs(output_crmsd_file)
    molecule_id = 0
    sg_dict = dict([("6w9c", f'$C2$'), ("6wrh", f'$P3_221$'), ("6yva", f'$P6_222$'), ("6wuu", f'$P2_1$'), ("6wx4", f'$I222$'),
                    ("6wzu", f'$P3_221$'), ("6xa9", f'$P4_12_12$'), ("6xaa", f'$P2_12_12$'), ("6xg3", f'$P3_221$'),
                    ("7jir", f'$I4_122$'), ("7jit", f'$I4_122$'), ("7jiw", f'$I4_122$'), ("7jn2", f'$I4_122$'), ("7jrn", f'$P2_1$'),
                    ("7cjd", f'$C2$'), ("7cjm", f'$I4_122$'), ("7cmd", f'$P2_1$'), ("7d47", f'$P3$'), ("7d6h", f'$P3_221$'),
                    ("7d7t", f'$P6_522$'), ("7koj", f'$I4_122$'), ("7kok", f'$I4_122$'), ("7kol", f'$I4_122$'), ("7krx", f'$I4_122$')])
    pdb = Pdb(list_of_pdbs[0], "is_ca", False)
    structure = pdb.create_structure(0)
    chain = structure.get_chain(0)
    # fasta_structure = chain.create_sequence()
    fasta_structure, coordinates_structure, residues_structure = get_sequence(structure, "A")
    with open("distances.csv", "a") as d:
        print("molecule_id", "pdbid", "chain", "molecule", "CA", "distance", "group", sep=",", file=d)
    for pdb_fname in list_of_pdbs[1:]:
        other_pdb = Pdb(pdb_fname, "is_ca", False)
        other_structure = other_pdb.create_structure(0)
        for ch in other_structure.chain_codes():
            molecule_id += 1
            fasta_other_structure, coordinates_other_structure, residues_other_structure = get_sequence(other_structure,
                                                                                                        ch)
            other_xyz = vector_core_data_basic_Vec3()
            other_chain = other_structure.get_chain(ch)
            xyz = vector_core_data_basic_Vec3()
            list_for_distance = []
            list_for_other_distance = []
            for aa in range(len(fasta_structure)):
                try:
                    # print(fasta_structure[aa], fasta_other_structure[aa])
                    if fasta_structure[aa] != '-' and fasta_other_structure[aa] != '-':
                        xyz.append(Vec3(coordinates_structure[aa][0]))
                        other_xyz.append(Vec3(coordinates_other_structure[aa][0]))
                        atom = coordinates_structure[aa][0]
                        other_atom = coordinates_other_structure[aa][0]
                        list_for_distance.append(residues_structure[aa])
                        list_for_other_distance.append(residues_other_structure[aa])
                        # list_of_rmsd.append(calculate_rmsd(atom, other_atom))
                except Exception as error:
                    print(error, file=sys.stderr)
            # print(xyz)
            # print(other_xyz)
            try:
                print("%s %s: %6.3f" % ((structure.code()).split(".")[0], (other_structure.code()).split(".")[0],
                                        rms.crmsd(xyz, other_xyz, len(xyz), True)))  # pdb_fname.split("/")[-1].split(".")[0]
                print()
            except Exception as error:
                print(error, file=sys.stderr)
                sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))
            distances = 0
            list_of_distances = []
            for aa in range(len(xyz)):
                # print("before rms.apply", xyz[aa], other_xyz[aa])
                rms.apply(xyz[aa])
                # print("after rms.apply", xyz[aa], other_xyz[aa])
                # with open("rotated_"+(other_structure.code()).split(".")[0]+".xyz", "a") as o: # To nie do końca działa tak jak powinno, bo 3 łańcuchy lądują w 1 pliku i brakuje nagłówka (309 i w kolejnej linii jakaś nazwa)
                #    print(f"{'  C'}{float(str(xyz[aa][0])):10.5f}{float(str(xyz[aa][1])):10.5f}{float(str(xyz[aa][2])):10.5f}", file=o)
                distance = f"{float(xyz[aa].distance_to(other_xyz[aa])):3.2f}"
                list_of_distances.append(distance)
                print(list_for_distance[aa], list_for_other_distance[aa], distance)
            distance_dict = dict(zip(list_for_distance, list_of_distances))
            # for key, value in distance_dict.items():
            # print(key, value)

            with open("distances.csv", "a") as d:
                for i in range(1, 321):
                    print(molecule_id, other_structure.code().split(".")[0], other_chain.id(), '"'+other_structure.code().split(".")[0]+" "+other_chain.id()+'"', i, distance_dict.get(i, "NaN"), sg_dict.get(str(other_structure.code().split(".")[0])), sep=",", file=d)
