import sys
import os

sys.path.append("/home/asia/DGront/bioshell/bin")
os.environ["BIOSHELL_DATA_DIR"] = '/home/asia/DGront/bioshell/data'
from pybioshell.core.data.io import Pdb
from pybioshell.core.data.basic import Vec3
from pybioshell.std import vector_core_data_basic_Vec3

from pybioshell.core.calc.structural.transformations import *
from pybioshell.utils import LogManager

LogManager.INFO()

"""
Script based on original PyBioShell script for crmsd calculation (ref to readthedocs)

ATTENTION! Scripts assumes that proteins are identical and have the same numerations of amino acids in pdb files. I takes only first chain from the file.

"""

rms = CrmsdOnVec3()
# list_of_pdbs = ['../INPUTS/6w9c.cif.pdb.pdb', '../INPUTS/6yva.cif.pdb.pdb']
list_of_pdbs = ['../INPUTS/6w9c.cif.pdb.pdb', '../INPUTS/6wrh.cif.pdb.pdb', '../INPUTS/6wuu.cif.pdb.pdb',
                '../INPUTS/6wx4.cif.pdb.pdb', '../INPUTS/6wzu.cif.pdb.pdb', '../INPUTS/6xa9.cif.pdb.pdb',
                '../INPUTS/6xaa.cif.pdb.pdb', '../INPUTS/6xg3.cif.pdb.pdb', '../INPUTS/6yva.cif.pdb.pdb',
                '../INPUTS/7cjd.cif.pdb.pdb', '../INPUTS/7cjm.cif.pdb.pdb', '../INPUTS/7cmd.cif.pdb.pdb',
                '../INPUTS/7d47.cif.pdb.pdb', '../INPUTS/7d6h.cif.pdb.pdb', '../INPUTS/7d7t.cif.pdb.pdb',
                '../INPUTS/7jir.cif.pdb.pdb', '../INPUTS/7jit.cif.pdb.pdb', '../INPUTS/7jiv.cif.pdb.pdb',
                '../INPUTS/7jiw.cif.pdb.pdb', '../INPUTS/7jn2.cif.pdb.pdb', '../INPUTS/7jrn.cif.pdb.pdb',
                '../INPUTS/7koj.cif.pdb.pdb', '../INPUTS/7kok.cif.pdb.pdb', '../INPUTS/7kol.cif.pdb.pdb',
                '../INPUTS/7krx.cif.pdb.pdb']


def get_sequence(bioshell_structure):
    for code in bioshell_structure.chain_codes():
        bioshell_chain = bioshell_structure.get_chain(code)
        prev_residue = bioshell_chain[0]
        fasta_list = []
        coordinates_list = []
        if prev_residue.id() != -5:
            for i in range(prev_residue.id() + 5):
                fasta_list.append('-')
                coordinates_list.append('-')
        for ires in range(int(bioshell_chain.terminal_residue_index()) + 1):
            residue = bioshell_chain[ires]
            difference = residue.id() - prev_residue.id()
            if difference > 1:
                for i in range(difference - 1):
                    fasta_list.append('-')
                    coordinates_list.append('-')
            letter_code = residue.residue_type().code1
            fasta_list.append(letter_code)
            coordinates_list.append(residue)
            # print(prev_residue.id(), residue.id(), difference)
            prev_residue = residue
        fasta_string = ''.join(fasta_list)
        w = str("> " + bioshell_structure.code() + "_" + bioshell_chain.id() + "\n" + fasta_string)
        print(w)
        return fasta_list, coordinates_list


def remove_old_outputs(filename):
    try:
        os.remove(filename)
    except OSError:
        pass


if __name__ == '__main__':
    output_crmsd_file = 'crmsd_results.txt'
    remove_old_outputs(output_crmsd_file)
    for i in range(len(list_of_pdbs)):
        pdb = Pdb(list_of_pdbs[i], "is_ca", False)
        structure = pdb.create_structure(0)
        chain = structure.get_chain(0)
        # fasta_structure = chain.create_sequence()
        fasta_structure, coordinates_structure = get_sequence(structure)
        for j in range(i + 1, (len(list_of_pdbs))):
            xyz = vector_core_data_basic_Vec3()
            other_pdb = Pdb(list_of_pdbs[j], "is_ca", False)
            other_structure = other_pdb.create_structure(0)
            fasta_other_structure, coordinates_other_structure = get_sequence(other_structure)
            other_xyz = vector_core_data_basic_Vec3()
            other_chain = other_structure.get_chain(0)
            for aa in range(len(fasta_structure)):
                try:
                    # print(fasta_structure[aa], fasta_other_structure[aa])
                    if fasta_structure[aa] != '-' and fasta_other_structure[aa] != '-':
                        xyz.append(Vec3(coordinates_structure[aa][0]))
                        other_xyz.append(Vec3(coordinates_other_structure[aa][0]))
                        atom = coordinates_structure[aa][0]
                        other_atom = coordinates_other_structure[aa][0]
                except Exception as error:
                    print(error, file=sys.stderr)
            try:
                with open(output_crmsd_file, 'a') as f:
                    print("%s %s %6.3f" % ((structure.code()).split(".")[0], (other_structure.code()).split(".")[0],
                                           rms.crmsd(xyz, other_xyz, len(xyz))),
                          file=f)  # pdb_fname.split("/")[-1].split(".")[0]

            except Exception as error:
                print(error, file=sys.stderr)
                sys.stderr.write(str(sys.exc_info()[0]) + " " + str(sys.exc_info()[1]))
