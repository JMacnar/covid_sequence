#!/usr/bin/env python

import sys, subprocess, re
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

INFILE = '../INPUTS/cmap/all.cmap'
OUTFILE = 'contact_map.png'
if len(sys.argv) > 2:
    OUTFILE = sys.argv[2]

x = []
y = []
cnts = []
nres = 0
data_array = np.zeros(shape=(321, 321))
for line in open(INFILE):
    if line.strip()[0] == '#': continue
    tokens = re.split("\s+", line.strip())
    #print(tokens)
    if len(tokens) < 7: continue
    distance = float(tokens[6])
    xi = int(tokens[2])
    yi = int(tokens[5])
    if max(xi, yi) > nres: nres = max(xi, yi)
    if abs(xi - yi) > 2:
        x.append(xi)
        y.append(yi)
        y.append(xi)
        x.append(yi)
        data_array[xi][yi] = float(tokens[6])
        if len(tokens) > 6:
            cnts.append(float(tokens[6]))
            cnts.append(float(tokens[6]))
        else:
            cnts.append(1.0)
            cnts.append(1.0)
sns.set(style="whitegrid")
sns.set_context("paper", font_scale=3.0)
#My_cmap = sns.color_palette("icefire", 5)
palette = ["#efedf5", "#8865bf", "#1496ba", "#c39eaa", "#98cd9a", 
           "#0888b7", "#08a2c7", "#07bdd8", "#07d7e8", "#07f2f9", 
           "#f9ac07", "#c77406", "#963b04", "#640303"]
My_cmap = sns.color_palette(palette, 5)
#print(data_array)
#print(len(x))
#print(cnts)
f, ax = plt.subplots(figsize=(15, 12))
sns.despine(f, left=True, bottom=True)
sns.heatmap(data_array, cmap=My_cmap) #Paired gist_stern_r
ax.set_ylabel("Residues")
ax.set_xlabel("Residues")
ax.set_xticks(range(0,321,20))
ax.set_yticks(range(0,321,20))
xlabels = []
for i in range(0, 321, 20):
    xlabels.append(i)
ax.set_xticklabels(xlabels)
ax.set_yticklabels(xlabels)
# fig, ax = plt.subplots()
# ax.scatter(x, y, c=cnts, marker=',', cmap='Blues')
ax.set_xlim((0, 320))
ax.set_ylim((0, 320))
# ax.set_aspect(1.0)
plt.savefig(OUTFILE, dpi=600)
plt.show()
# plt.close(fig)
