import seaborn as sns
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import csv


def numpy_fillna(data):
    # Get lengths of each row of data
    lens = np.array([len(i) for i in data])

    # Mask of valid places in each row
    mask = np.arange(lens.max()) < lens[:, None]

    # Setup output array and put elements from data into masked positions
    out = np.zeros(mask.shape, dtype=data.dtype)
    out[mask] = np.concatenate(data)
    return out


data_file = '../INPUTS/plpro_COVID_MR_models.csv'
data = pd.read_csv(data_file, sep=' ', dtype={"P(Q1) of resulting structure": float, "P(Q1) of MR Model": float},
                   parse_dates=["Deposition"], dayfirst=True)
data.head(n=3)
data.loc[:, 'Deposition date'] = data['Deposition']
data['Deposition date'] = data['Deposition date'].dt.strftime('%Y-%m-%d').astype(str)
sns.set(style="whitegrid")
sns.set_context("paper", font_scale=2.0)
f, ax = plt.subplots(figsize=(7.5, 7.5))
legend_names = list(set(data['Deposition date'].tolist()))
ln_sorted = legend_names.sort()
print(legend_names)
sns.despine(f, left=True, bottom=True)
ax.set_xlim(xmin=0, xmax=100)
ax.set_ylim(ymin=0, ymax=100)
# plt.legend(loc="upper left")
# fig = sns.lmplot(data=data, x="Resolution", y="PQ1", hue="Protein")
sns.scatterplot(data=data, x="P(Q1) of resulting structure", y="P(Q1) of MR Model", hue="Deposition date",
                hue_order=legend_names, palette="dark:salmon", linewidth=0, ax=ax, legend=False, s=150, alpha=0.7)
# fig.set_xlabel("Resolution [A]")
# fig.set_ylabel("P(Q1)")
# plt.legend(bbox_to_anchor=(1.05, 1), loc='upper left', title="Deposition date")
#f.subplots_adjust(right=0.75)

# fig = plt.figure(num=None, figsize=(10, 10), dpi=80, facecolor='w', edgecolor='k')
# cmap = sns.cubehelix_palette(light=1, as_cmap=True)
# labels = pd.read_csv('../OUTPUTS/X-ray_sequences.txt', delimiter=',')
# res = sns.heatmap(df, annot=labels, vmin=0.0, vmax=100.0,
#                  fmt='', cmap=cmap, cbar_kws={"shrink": .82},
#                  linewidths=0.1, linecolor='gray')
# res.invert_yaxis()
plt.savefig('PQ1_PQ1.png', dpi=600)
plt.show()
# plt.savefig('PQ1_PQ1.png')
print("done")
