#!/usr/bin/env python
import pandas as pd
import sys, re
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from pylab import *
from matplotlib.ticker import MultipleLocator

INFILE = '../INPUTS/ppl-X-ray_b_avgs_95_v5.csv'
OUTFILE = 'B-f_map.png'
if len(sys.argv) > 2:
    OUTFILE = sys.argv[2]

#sns.set(style="whitegrid")
#sns.set_context("paper", font_scale=1.5)
bfactors_df = pd.DataFrame()
bfactors_df = pd.read_csv('../INPUTS/ppl-X-ray_b_avgs_95_v5.csv', sep=';')  # na_values=["NaN"], keep_default_na=False)
bfactors_df.head(n=3)
print(bfactors_df)
bfactors_df = bfactors_df.reset_index(drop=True)
bfactors_df = bfactors_df.set_index("PDBid")
# bfactors_df.drop('PDBid', axis=1)
bfactors_df.fillna(1000)

sns.set(style="whitegrid")
sns.set_context("paper", font_scale=3)
# My_cmap = sns.color_palette("icefire", 5)
palette = ["#efedf5", "#8865bf", "#1496ba", "#c39eaa", "#98cd9a",
           "#0888b7", "#08a2c7", "#07bdd8", "#07d7e8", "#07f2f9",
           "#f9ac07", "#c77406", "#963b04", "#640303"]
My_cmap = sns.color_palette(palette, 5)
# print(data_array)
# print(len(x))
# print(cnts)
f, ax = plt.subplots(figsize=(120, 20))
sns.despine(f, left=True, bottom=True)
sns.heatmap(bfactors_df, cmap="Blues", fmt=".0f", annot=False)  # Paired gist_stern_r
axvspan(60, 61, facecolor='r', alpha=0.15, linewidth=3)
axvspan(25, 26, facecolor='grey', alpha=0.15, linewidth=3)
axvspan(12, 13, facecolor='grey', alpha=0.15, linewidth=3)
axvspan(143, 144, facecolor='grey', alpha=0.15, linewidth=3)
axvspan(84, 85, facecolor='grey', alpha=0.15, linewidth=3)
ax.set_xlabel("Residues")
ax.set_ylabel("PDBid")
# ax.set_xticks(range(-1,326,20))
# ax.set_yticks(range(0,321,20))
xlabels = []
# for i in range(-1, 326, 20):
#    xlabels.append(i)
# ax.set_xticklabels(xlabels)
# ax.set_yticklabels(xlabels)
# fig, ax = plt.subplots()
# ax.scatter(x, y, c=cnts, marker=',', cmap='Blues')
ax.set_xlim((-1, 325))
#ax.set_ylim((0,1))
x = [1] * 320
x2 = [1] * 320
#ax.plot(x, x2, color='grey')
beta_kartka = list(range(2, 6)) + list(range(32, 36)) + list(range(50, 51)) + list(range(61, 62)) + list(range(83, 93)) + list(range(107, 111)) + list(range(129,132)) + list(range(186, 187)) + list(range(190, 191))
# beta_kartka_chainB=[i + 246 for i in beta_kartka]
helisa = list(range(10, 17)) + list(range(20, 27)) + list(range(38, 41)) + list(range(64, 78)) + list(range(96, 102)) + list(range(120, 126)) + list(range(142,153)) + list(range(194, 198)) + list(range(201, 219)) + list(range(221, 225)) + list(range(231, 244))
# helisa_chainB=[i + 246 for i in helisa]
ax.scatter(beta_kartka, [1] * len(beta_kartka), marker='>', s=100, color='yellow')
# m.plot(beta_kartka_chainB,[1]*len(beta_kartka_chainB),'y>')
ax.plot(helisa, [1] * len(helisa), 'gv')
# m.plot(helisa_chainB,[1]*len(helisa_chainB),'gv')
# ax.set_ylim((-1, 325))
# ax.set_aspect(1.0)
# plt.savefig(OUTFILE, dpi=600)
plt.show()
# plt.close(fig)
